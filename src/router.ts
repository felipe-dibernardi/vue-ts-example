import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import store from './store';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      beforeEnter (to, from, next) {
        if (localStorage.getItem('token')) {
          next();
        } else {
          next('/login');
        }
      }
    },
    {
      path: '/portfolios',
      name: 'portfolios',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Portfolios.vue'),
      beforeEnter (to, from, next) {
        if (localStorage.getItem('token')) {
          next();
        } else {
          next('/login');
        }
      }
    },
    {
      path: '/stocks',
      name: 'stocks',
      component: () => import('./views/Stocks.vue'),
      beforeEnter (to, from, next) {
        if (localStorage.getItem('token')) {
          next();
        } else {
          next('/login');
        }
      }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('./views/Login.vue')
    }
  ],
});
