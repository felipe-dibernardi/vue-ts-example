import { Portfolio } from "@/model/Portfolio";
import { PortfolioState } from './types';
import { Module, GetterTree, ActionTree, MutationTree } from 'vuex';
import { RootState } from '../types';
import { Stock } from '@/model/Stock';
import { StockOrder } from '@/model/StockOrder';

const state: PortfolioState = {
    portfolios: []
}

const getters: GetterTree<PortfolioState, RootState> = {
    portfolios(state) {
        return state.portfolios        
    }
}

const actions: ActionTree<PortfolioState, RootState> = {
    sellPortfolio(context, order: StockOrder) {
        context.commit('sellStock', order)
        context.commit('setFunds', context.rootGetters.funds + (order.stock.value * order.quantity), { root: true })
    }
}

const mutations: MutationTree<PortfolioState> = {
    setPortfolio(state: PortfolioState, payload: Portfolio[]) {
        state.portfolios = payload;
    },
    buyStock(state, order: StockOrder) {
        let portItem = state.portfolios.find(element => element.stock.name == order.stock.name);
        if (portItem) {
            portItem.quantity += order.quantity;
        } else {
            state.portfolios.push(new Portfolio(order.stock, order.quantity));
        }
    },
    sellStock(state, order: StockOrder) {
        let portItem = state.portfolios.find(element => element.stock.name == order.stock.name);
        if (portItem) {
            portItem.quantity -= order.quantity;
            if (portItem.quantity === 0) {
                state.portfolios.splice(state.portfolios.indexOf(portItem), 1);
            }
        }
        
    },
    updatePortfolios(state, stocks: Stock[]) {
        return state.portfolios.map(portfolio => {
            let record = stocks.find((element: Stock) => element.name == portfolio.stock.name);
            if (record) {
                if (record.value !== portfolio.stock.value) {
                    portfolio.stock.value = record.value;
                }
            }
            return portfolio;
        })
    }
}

const namespaced: boolean = true;

export const portfolios: Module<PortfolioState, RootState> = {
    namespaced,
    state,
    getters,
    actions,
    mutations
    
}