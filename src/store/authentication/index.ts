import { AuthenticationState } from "./types";
import { GetterTree, MutationTree, ActionTree, Module } from 'vuex';
import { RootState } from '../types';
import { authService } from '@/service/AuthService';
import router from '@/router';

const state: AuthenticationState = {
    token: '',
    username: '',
    expirationDate: null
}

const getters: GetterTree<AuthenticationState, RootState> = {
    token(state) {
        return state.token;
    },
    username(state) {
        return state.username;
    },
    expirationDate(state) {
        return state.expirationDate;
    },
    authenticated(state): boolean {
        return state.token != '';
    }
}

const mutations: MutationTree<AuthenticationState> = {
    setAuthAttributes(state, {token, username, expirationDate}) {
        state.token = token;
        state.username = username;
        state.expirationDate = expirationDate;
    },
    setExpirationDate(state, expirationDate: Date) {
        state.expirationDate = expirationDate;
    }
}

const actions: ActionTree<AuthenticationState, RootState> = {
    login(context, credentials) {
        authService.login(credentials)
        .then(resp => {
            let token = resp.headers['authorization'].split(" ")[1];
            let payload = JSON.parse(atob(token.split('.')[1]));
            localStorage.setItem('token', token);
            localStorage.setItem('username', payload.sub);
            localStorage.setItem('expirationDate', '' + new Date(payload.exp * 1000));
            context.commit('setAuthAttributes', {
                token,
                username: payload.sub,
                expirationDate: new Date(payload.exp)
            });
            router.push({name: 'home'})
        })
        .catch(err => console.log(err));
    },
    logout(context) {
        localStorage.removeItem('token');
        localStorage.removeItem('username');
        localStorage.removeItem('expirationDate');
        router.push({name: 'login'});
        context.commit('setAuthAttributes', {
            token: '',
            username: '',
            expirationDate: null
        });
    },
    tryAutoLogin(context) {
        let token = localStorage.getItem('token');
        if (!token) {
            return;
        }
        let expirationDate = localStorage.getItem('expirationDate');
        if (new Date(expirationDate!) <= new Date()) {
            return;
        }
        let username = localStorage.getItem('username');
        context.commit('setAuthAttributes', {token, expirationDate, username});
    }
}

const namespaced: boolean = true;

export const authentication:Module<AuthenticationState, RootState> = {
    namespaced,
    state,
    getters,
    actions,
    mutations
}