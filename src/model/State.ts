import { Stock } from './Stock';
import { Portfolio } from './Portfolio';

export default class State {
    constructor(public stocks: Stock[], public stockPortfolio: Portfolio[], public funds: number) {}
}